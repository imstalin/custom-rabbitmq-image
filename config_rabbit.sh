#!/bin/bash

# This script needs to be executed just once
if [ -f /$0.completed ] ; then
  echo "$0 `date` /$0.completed found, skipping run"
  exit 0
fi

# Wait for RabbitMQ startup
for (( ; ; )) ; do
  sleep 5
  rabbitmqctl -q node_health_check > /dev/null 2>&1
  if [ $? -eq 0 ] ; then
    echo "$0 `date` rabbitmq is now running"
    break
  else
    echo "$0 `date` waiting for rabbitmq startup"
  fi
done

# Execute RabbitMQ config commands here

# Create user

rabbitmqctl add_user guestqa guest@1

#Set user tags
rabbitmqctl set_user_tags guestqa administrator

#add virtual host
rabbitmqctl add_vhost  myhost

#add permission to host
rabbitmqctl set_permissions -p myhost guestqa ".*" ".*" ".*"
  
#Enable management plugin 
rabbitmq-plugins enable --offline rabbitmq_mqtt rabbitmq_federation_management rabbitmq_stomp

# Create mark so script is not ran again
touch /$0.completed
